/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IOfflineStreamOperator.idl
 *
 * @brief 离线流的操作接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_0
 *
 * 引用：ohos.hdi.camera.v1_0.Types
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief 定义Camera设备离线流操作。
 *
 * 对Camera设备离线流执行取消捕获和释放操作。
 *
 * @since 3.2
 * @version 1.0
 */
interface IOfflineStreamOperator {
     /**
     * @brief 取消捕获请求。
     *
     * @param captureId 用于标识要取消的捕获请求。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    CancelCapture([in] int captureId);

     /**
     * @brief 释放离线流。
     *
     * @param streamIds 用于标识要释放的多条离线流。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseStreams([in] int[] streamIds);

     /**
     * @brief 释放所有离线流。
     *
     * 释放流的前置条件：
     *
     * 1.所有单次捕获的Capture处理完成。
     *
     * 2.所有连续捕获请求都已经被CancelCapture。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    Release();
}
/** @} */