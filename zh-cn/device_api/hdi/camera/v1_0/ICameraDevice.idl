/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraDevice.idl
 *
 * @brief Camera设备操作接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_0
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.IStreamOperatorCallback
 * - ohos.hdi.camera.v1_0.IStreamOperator
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.IStreamOperatorCallback;
import ohos.hdi.camera.v1_0.IStreamOperator;

/**
 * @brief 定义Camera设备基本的操作。
 *
 * 设置流回调接口、更新控制参数、执行metadata相关操作。
 *
 * @since 3.2
 * @version 1.0
 */
interface ICameraDevice {
     /**
     * @brief 获取流操作句柄。
     *
     * @param callbackObj 设置流回调接口，详细可查看{@link IStreamOperatorCallback}，
     * 用于上报捕获开始{@link OnCaptureStarted}，捕获结束{@link OnCaptureEnded}，
     * 捕获错误等信息{@link OnCaptureError}。
     * @param streamOperator 返回流操作句柄。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetStreamOperator([in] IStreamOperatorCallback callbackObj, [out] IStreamOperator streamOperator);

    /**
     * @brief 更新设备控制参数。
     *
     * @param settings Camera设置参数，包括sensor帧率，3A相关参数等。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    UpdateSettings([in] unsigned char[] settings);

     /**
     * @brief 设置metadata上报模式，逐帧上报还是设备状态变化时上报。
     *
     * @param mode metadata的上报模式，逐帧上报或者设备状态变化时上报，查看{@link ResultCallbackMode}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetResultMode([in] enum ResultCallbackMode mode);

     /**
     * @brief 查询使能的metadata。
     *
     * {@link EnableResult}使能需要上报的metadata之后，可通过此接口查询使能的metadata。
     *
     * @param results 所有使能的metadata的ID数组。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    GetEnabledResults([out] int[] results);

     /**
     * @brief 打开metadata上报开关。
     *
     * {@link OnResult}只上报此接口使能后的metadata。
     *
     * @param results 需要打开上报开关的多个metadata。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see DisableResult
     *
     * @since 3.2
     * @version 1.0
     */
    EnableResult([in] int[] results);

     /**
     * @brief 关闭metadata上报开关。
     *
     * 屏蔽之后，相应的{@link OnResult}不再上报，需{@link EnableResult}使能之后才上报。
     *
     * @param results 需要关闭上报开关的metadata。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see EnableResult
     *
     * @since 3.2
     * @version 1.0
     */
    DisableResult([in] int[] results);

     /**
     * @brief 关闭当前Camera设备。
     *
     * @see OpenCamera
     *
     * @since 3.2
     * @version 1.0
     */
    Close();
}
/** @} */