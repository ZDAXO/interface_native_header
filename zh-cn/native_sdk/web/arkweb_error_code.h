/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Web
 * @{
 *
 * @brief 为ArkWeb NDK接口发生异常提供错误码。
 * @since 12
 */
/**
 * @file arkweb_error_code.h
 *
 * @brief 声明ArkWeb NDK接口异常错误码。
 * @library libohweb.so
 * @syscap SystemCapability.Web.Webview.Core
 * @since 12
 */
#ifndef ARKWEB_ERROR_CODE_H
#define ARKWEB_ERROR_CODE_H

/**
 * @brief 定义ArkWeb NDK接口异常错误码。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 12
 */
typedef enum ArkWeb_ErrorCode {
/** 未知错误。 */
ARKWEB_ERROR_UNKNOWN = 17100100,

/** 参数无效。 */
ARKWEB_INVALID_PARAM = 17100101,

/** 注册scheme的配置失败，应该在创建ArkWeb之前注册。 */
ARKWEB_SCHEME_REGISTER_FAILED = 17100102,
} ArkWeb_ErrorCode;

#endif // ARKWEB_ERROR_CODE_H
