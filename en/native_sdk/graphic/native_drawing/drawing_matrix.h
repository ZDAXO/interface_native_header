/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_MATRIX_H
#define C_INCLUDE_DRAWING_MATRIX_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_matrix.h
 *
 * @brief Declares the functions related to the matrix in the drawing module.
 *
 * File to include: native_drawing/drawing_matrix.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Matrix</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Matrix</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreate(void);

/**
 * @brief Creates an <b>OH_Drawing_Matrix</b> with the rotation attribute.
 * The matrix is obtained by rotating an identity matrix by a given degree around the rotation point (x, y).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param deg Angle to rotate, in degrees. A positive number indicates a clockwise rotation,
 * and a negative number indicates a counterclockwise rotation.
 * @param x Coordinate point on the X axis.
 * @param y Coordinate point on the Y axis.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateRotation(float deg, float x, float y);

/**
 * @brief Creates an <b>OH_Drawing_Matrix</b> with the scale attribute.
 * The matrix is obtained by scaling an identity matrix with the factor (sx, sy) at the rotation point (px, py).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param sx Horizontal scale factor.
 * @param sy Vertical scale factor.
 * @param px Coordinate point on the X axis.
 * @param py Coordinate point on the Y axis.
 * @return Returns the pointer to the {@link OH_Drawing_Matrix} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateScale(float sx, float sy, float px, float py);

/**
 * @brief Creates an <b>OH_Drawing_Matrix</b> with the translation attribute.
 * The matrix is obtained by translating the identity matrix by the distance (dx, dy).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param dx Horizontal distance to translate.
 * @param dy Vertical distance to translate.
 * @return Returns the pointer to the {@link OH_Drawing_Matrix} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Matrix* OH_Drawing_MatrixCreateTranslation(float dx, float dy);

/**
 * @brief Sets matrix parameters for an <b>OH_Drawing_Matrix</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an <b>OH_Drawing_Matrix</b> object.
 * @param scaleX Horizontal scale factor.
 * @param skewX Horizontal skew coefficient.
 * @param transX Horizontal translation coefficient.
 * @param skewY Vertical skew coefficient.
 * @param scaleY Vertical scale factor.
 * @param transY Vertical translation coefficient.
 * @param persp0 Perspective coefficient of the X axis.
 * @param persp1 Perspective coefficient of the Y axis.
 * @param persp2 Perspective scale coefficient.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixSetMatrix(OH_Drawing_Matrix*, float scaleX, float skewX, float transX,
    float skewY, float scaleY, float transY, float persp0, float persp1, float persp2);

/**
 * @brief Multiplies two matrices to produce a new matrix.
 * For example, if a given matrix a and a given matrix b are shown as follows,
 *                    | A B C |          | J K L |
 *                a = | D E F |,     b = | M N O |
 *                    | G H I |          | P Q R |
 * then the final matrix total is as follows:
 *                            | A B C |   | J K L |   | AJ+BM+CP AK+BN+CQ AL+BO+CR |
 *           total = a * b =  | D E F | * | M N O | = | DJ+EM+FP DK+EN+FQ DL+EO+FR |
 *                            | G H I |   | P Q R |   | GJ+HM+IP GK+HN+IQ GL+HO+IR |
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param total Pointer to the new matrix, which is an {@link OH_Drawing_Matrix} object.
 * @param a Pointer to the first matrix, which is an {@link OH_Drawing_Matrix} object.
 * @param b Pointer to the second matrix, which is an {@link OH_Drawing_Matrix} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixConcat(OH_Drawing_Matrix* total, const OH_Drawing_Matrix* a,
    const OH_Drawing_Matrix* b);

/**
 * @brief Obtains a matrix value of a given index. The index ranges from 0 to 8.
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param index Index. The value ranges from 0 to 8.
 * @return Returns the matrix value.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_MatrixGetValue(OH_Drawing_Matrix*, int index);

/**
 * @brief Sets a matrix as an identity matrix and rotates it by a given degree around the rotation point (px, py).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param degree Angle to rotate, in degrees. A positive number indicates a clockwise rotation,
 * and a negative number indicates a counterclockwise rotation.
 * @param px Coordinate point on the X axis.
 * @param py Coordinate point on the Y axis.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixRotate(OH_Drawing_Matrix*, float degree, float px, float py);

/**
 * @brief Sets a matrix as an identity matrix and translates it by a given distance (dx, dy).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param dx Horizontal distance to translate.
 * @param dy Vertical distance to translate.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixTranslate(OH_Drawing_Matrix*, float dx, float dy);

/**
 * @brief Sets a matrix as an identity matrix and scales it with the factor (sx, sy) at the rotation point (px, py).
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param sx Horizontal scale factor.
 * @param sy Vertical scale factor.
 * @param px Coordinate point on the X axis.
 * @param py Coordinate point on the Y axis.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_MatrixScale(OH_Drawing_Matrix*, float sx, float sy, float px, float py);

/**
 * @brief Inverts a matrix and returns the result.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param inverse Pointer to the matrix to invert, which is an {@link OH_Drawing_Matrix} object.
 * The object can be created by calling {@link OH_Drawing_MatrixCreate}.
 * @return Returns <b>true</b> if the matrix is reversible and the passed-in <b>inverse</b> is inverted;
 * returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixInvert(OH_Drawing_Matrix*, OH_Drawing_Matrix* inverse);

/**
 * @brief Generates a transform matrix by setting source points and destination points.
 * Both the number of source points and that of destination points must be in the range [0, 4].
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param src Array of source points.
 * @param dst Array of destination points. The number of destination points must be the same as that of source points.
 * @param count Number of source points or destination points.
 * @return Returns <b>true</b> if the matrix is generated; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixSetPolyToPoly(OH_Drawing_Matrix*, const OH_Drawing_Point2D* src,
    const OH_Drawing_Point2D* dst, uint32_t count);

/**
 * @brief Checks whether two <b>OH_Drawing_Matrix</b> objects are equal.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to a matrix, which is an {@link OH_Drawing_Matrix} object.
 * @param other Pointer to the other matrix, which is an {@link OH_Drawing_Matrix} object.
 * @return Returns <b>true</b> if the two matrices are equal; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsEqual(OH_Drawing_Matrix*, OH_Drawing_Matrix* other);

/**
 * @brief Checks whether an <b>OH_Drawing_Matrix</b> object is an identity matrix.
 * The identity matrix is as follows: | 1 0 0 |
 *              | 0 1 0 |
 *              | 0 0 1 |
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @return Returns <b>true</b> if the matrix is an identity matrix; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_MatrixIsIdentity(OH_Drawing_Matrix*);

/**
 * @brief Destroys an <b>OH_Drawing_Matrix</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Matrix Pointer to an <b>OH_Drawing_Matrix</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_MatrixDestroy(OH_Drawing_Matrix*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
