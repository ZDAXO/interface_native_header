/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup VideoEncoder
 * @{
 *
 * @brief Provides the functions and enums for video encoding.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 *
 * @since 9
 * @version 1.0
 */

/**
 * @file native_avcodec_videoencoder.h
 *
 * @brief Declares the native APIs used for video encoding.
 *
 * @since 9
 * @version 1.0
 */

#ifndef NATIVE_AVCODEC_VIDEOENCODER_H
#define NATIVE_AVCODEC_VIDEOENCODER_H

#include <stdint.h>
#include <stdio.h>
#include "native_averrors.h"
#include "native_avformat.h"
#include "native_avmemory.h"
#include "native_avcodec_base.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates a video encoder instance based on a Multipurpose Internet Mail Extension (MIME) type.
 * This API is recommended in most cases.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param mime Indicates the pointer to a MIME type. For details, see {@link OH_AVCODEC_MIMETYPE_VIDEO_AVC}.
 * @return Returns the pointer to an <b>OH_AVCodec</b> instance.
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByMime(const char *mime);

/**
 * @brief Creates a video encoder instance based on a video encoder name. 
 * To use this API, you must know the exact name of the video encoder.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param name Indicates the pointer to a video encoder name.
 * @return Returns the pointer to an <b>OH_AVCodec</b> instance.
 * @since 9
 * @version 1.0
 */
OH_AVCodec *OH_VideoEncoder_CreateByName(const char *name);

/**
 * @brief Clears the internal resources of a video encoder and destroys the video encoder instance.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Destroy(OH_AVCodec *codec);

/**
 * @brief Sets an asynchronous callback so that your application can respond to events generated by a video encoder.
 * This API must be called prior to <b>Prepare</b>.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param callback Indicates a collection of all callback functions. For details, see {@link OH_AVCodecAsyncCallback}.
 * @param userData Indicates the pointer to user-specific data.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetCallback(OH_AVCodec *codec, OH_AVCodecAsyncCallback callback, void *userData);

/**
 * @brief Configures a video encoder. Typically, you need to configure the attributes of the video track that can be encoded.
 * This API must be called prior to <b>Prepare</b>.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance, 
 * which provides the attributes of the video track to be encoded.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Configure(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief Prepares internal resources for a video encoder. This API must be called after <b>Configure</b>.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Prepare(OH_AVCodec *codec);

/**
 * @brief Starts a video encoder. This API can be called only after the encoder is prepared successfully.
 * After being started, the encoder starts to report the {@link OH_AVCodecOnNeedInputData} event.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Start(OH_AVCodec *codec);

/**
 * @brief Stops a video encoder. After the encoder is stopped, you can call <b>Start</b> to start it again.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Stop(OH_AVCodec *codec);

/**
 * @brief Clears the input and output data in the internal buffer of a video encoder.
 * This API invalidates the indexes of all buffers previously reported through the asynchronous callback.
 * Therefore, before calling this API, ensure that the buffers corresponding to the indexes are no longer required.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Flush(OH_AVCodec *codec);

/**
 * @brief Resets a video encoder. To continue encoding, you must call <b>Configure</b> and <b>Start</b>
 * to configure and start the encoder again.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_Reset(OH_AVCodec *codec);

/**
 * @brief Obtains the attributes of the output data of a video encoder. The <b>OH_AVFormat</b> instance
 * in the return value will become invalid when this API is called again or 
 * when the <b>OH_AVCodec</b> instance is destroyed.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns the pointer to an <b>OH_AVFormat</b> instance.
 * @since 9
 * @version 1.0
 */
OH_AVFormat *OH_VideoEncoder_GetOutputDescription(OH_AVCodec *codec);

/**
 * @brief Sets dynamic parameters for a video encoder. This API can be called only after the encoder is started.
 * Incorrect parameter settings may cause encoding failure.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param format Indicates the handle to an <b>OH_AVFormat</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_SetParameter(OH_AVCodec *codec, OH_AVFormat *format);

/**
 * @brief Obtains an input surface from a video encoder. This API must be called prior to <b>Prepare</b>.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param window Indicates the double pointer to an <b>OHNativeWindow</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_GetSurface(OH_AVCodec *codec, OHNativeWindow **window);

/**
 * @brief Frees an output buffer of a video encoder.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @param index Indicates the index of an output buffer.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_FreeOutputData(OH_AVCodec *codec, uint32_t index);

/**
 * @brief Notifies a video encoder that input streams end. This API is recommended in surface mode.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @param codec Indicates the pointer to an <b>OH_AVCodec</b> instance.
 * @return Returns <b>AV_ERR_OK</b> if the operation is successful.
 * @return Returns an error code defined in {@link OH_AVErrCode} if the operation fails.
 * @since 9
 * @version 1.0
 */
OH_AVErrCode OH_VideoEncoder_NotifyEndOfStream(OH_AVCodec *codec);

/**
 * @brief Enumerates the bit rate modes of video encoding.
 * 
 * @syscap SystemCapability.Multimedia.Media.VideoEncoder
 * @since 9
 * @version 1.0
 */
typedef enum OH_VideoEncodeBitrateMode {
    /** Constant bit rate. */
    CBR = 0,
    /** Variable bit rate. */
    VBR = 1,
    /** Constant quality. */
    CQ = 2,
} OH_VideoEncodeBitrateMode;

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVCODEC_VIDEOENCODER_H
